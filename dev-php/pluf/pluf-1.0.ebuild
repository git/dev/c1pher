# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
MY_P="${PN}-v${PV}"

DESCRIPTION="A PHP WebApp Framework"
HOMEPAGE="http://pluf.org/"
SRC_URI="http://projects.ceondo.com/p/${PN}/source/download/v${PV} -> ${MY_P}.zip"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-arch/unzip"
RDEPEND="dev-lang/php
	dev-php/PEAR-Console_Getopt
	dev-php/PEAR-Mail
	dev-php/PEAR-Mail_Mime
	${DEPEND}"
S="${WORKDIR}/${MY_P}"

src_install() {
	insinto /usr/share/php/${PN}
	doins -r src/*
}
