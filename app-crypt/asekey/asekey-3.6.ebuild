# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-crypt/asekey/asekey-3.4.ebuild,v 1.2 2009/05/16 07:30:46 robbat2 Exp $
EAPI="2"
inherit eutils

DESCRIPTION="ASEKey USB SIM Card Reader"
HOMEPAGE="http://www.athena-scs.com"
SRC_URI="http://www.athena-scs.com/downloads/${P}.tar.bz2"
LICENSE="BSD LGPL-2.1"
SLOT="0"
IUSE=""
KEYWORDS=""
RDEPEND=">=sys-apps/pcsc-lite-1.3.0[usb]
	=virtual/libusb-0*"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

src_prepare() {
	epatch "${FILESDIR}/${P}-install-driver-bundle.patch" \
		"${FILESDIR}/${PN}-3.4-remove-ifdhcreatechannel-counter-constrain.patch" \
		"${FILESDIR}/${P}-install-udev-rules.patch" \
		|| die "Patching failed"
	sed -i -e 's:GROUP="pcscd":ENV{PCSCD}="1":' "92_pcscd_${PN}.rules" || die
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc ChangeLog README || die
}

